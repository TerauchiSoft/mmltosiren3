﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMLToPockecom
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.octaveTextBox.Text = NowStatus.Instance.NowOctave.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string text = this.mmlTextBox.Text;
            var converter = new MMLConverter();
            NowStatus.Instance.NowOctave = int.Parse(this.octaveTextBox.Text);
            var (bytes, bytesTexts) = converter.Convert(text, this.checkAllRest.Checked);
            this.resultTextbox.Text = bytesTexts;
        }
    }
}
