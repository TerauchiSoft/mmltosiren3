﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMLToPockecom
{
    internal static class LengthConverter
    {
        public static int ConvertToPokecom(string valueText)
        {
            string valueStr = valueText.Replace(".", string.Empty);
            int value = int.Parse(valueStr);
            int transedValue = GetTransValue(value);

            if (valueText.Contains("."))
            {
                // 付点音符分追加
                transedValue += GetTransValue(value * 2);
            }

            return transedValue;
        }
        
        /// <summary>
        /// 音符表示 → 実際長さへの変換
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static int GetTransValue(int value)
        {
            switch (value)
            {
                case 1:
                    return LengthConstant.LENGTH_1;
                case 2:
                    return LengthConstant.LENGTH_2;
                case 4:
                    return LengthConstant.LENGTH_4;
                case 8:
                    return LengthConstant.LENGTH_8;
                case 16:
                    return LengthConstant.LENGTH_16;
                case 32:
                    return LengthConstant.LENGTH_32;
                default:
                    return 0;
            }

        }
    }
}
