﻿namespace MMLToPockecom
{
    internal class OperatorConstant
    {
        /// <summary>
        /// 音階原点
        /// </summary>
        public static byte COMMAND_NOTE_BASE = 0x00;

        /// <summary>
        /// 休符
        /// </summary>
        public static byte COMMAND_REST = 0x50;

        /// <summary>
        /// パルス幅(互換性のため)
        /// </summary>
        public static byte COMMAND_PULSE = 0x51;

        /// <summary>
        /// 再開位置設定
        /// </summary>
        public static byte COMMAND_RESTART = 0x52;

        /// <summary>
        /// 再開
        /// </summary>
        public static byte COMMAND_LOOP = 0x53;

        /// <summary>
        /// 音量
        /// </summary>
        public static byte COMMAND_VOLUME = 0x54;

        /// <summary>
        /// キーオン時の音量変化速度
        /// </summary>
        public static byte COMMAND_VOLUME_ON = 0x55;

        /// <summary>
        /// キーオフ時の音量変化速度
        /// </summary>
        public static byte COMMAND_VOLUME_OFF = 0x56;

        /// <summary>
        /// 終了
        /// </summary>
        public static byte COMMAND_END = 0xff;

    }
}
