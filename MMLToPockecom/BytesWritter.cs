﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMLToPockecom
{
    internal class BytesWritter
    {
        public string GetTexts(List<byte> bytes)
        {
            StringBuilder sb = new StringBuilder();
            int index = 0;
            foreach (byte b in bytes)
            {
                if (index == 0)
                {
                    sb.Append(".db ");
                }
                sb.Append("0x");
                string s = Convert.ToString(b, 16);
                sb.Append(s);
                sb.Append(",");
                if (index > 0 && index % 8 == 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                    sb.Append(Environment.NewLine);
                    index = -1;
                }
                index++;
            }
            if (sb[sb.Length - 1] == ',')
            {
                sb.Remove(sb.Length - 1, 1);
            }
            return sb.ToString();
        }
    }
}
