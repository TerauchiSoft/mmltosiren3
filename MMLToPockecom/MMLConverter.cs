﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace MMLToPockecom
{
    internal class MMLConverter
    {
        private BytesWritter bytesWritter = new BytesWritter();

        public (List<byte>, string) Convert(string text, bool isCheckAllRest)
        {
            text = text.Replace("\n", "");
            string[] mmls = text.Split('\r');

            List<byte> operators = new List<byte>();
            StringBuilder texts = new StringBuilder();
            for (int index = 0; index < mmls.Length; index++)
            {
                string mmlText = mmls[index];
                if (mmlText.Length <= 11)
                {
                    continue;
                }

                string part = mmlText.Substring(0, 11).Replace("/*M ", string.Empty);
                part = part.Replace("*/", string.Empty).Trim();

                mmlText = mmlText.Remove(0, 11);
                var commands = CommandGetter.GetCommands(mmlText, isCheckAllRest);
                operators.AddRange(commands);

                var bytesText = bytesWritter.GetTexts(commands);
                texts.Append(string.Format(";part {0}", part));
                texts.Append(Environment.NewLine);
                texts.Append(bytesText);
                texts.Append(Environment.NewLine);
            }
            return (operators, texts.ToString());
        }
    }
}
