﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MMLToPockecom
{
    static internal class CommandGetter
    {
        private static bool isOutputNoteIsRest;

        private static bool isSlar;

        public static List<byte> GetCommands(string commandText, bool isCheckAllRest)
        {
            isSlar = false;
            isOutputNoteIsRest = isCheckAllRest;
            commandText = commandText.ToLower();


            var commands = new List<byte>();
            for (int index = 0; index < commandText.Length; index++)
            {
                NowStatus.Instance.ResetNote(); // 一音ごとに＃♭をリセット
                int octave = NowStatus.Instance.NowOctave;
                int note = (octave - 1) * LengthConstant.NOTE_NUM;
                var prevNote = NowStatus.Instance.NowNote;

                char command = commandText[index];
                switch (command)
                {
                    case 't':
                        ++index;
                        string tempo = GetNumeric.Get(commandText, ref index);
                        // TODO;テンポ実装なし
                        break;
                    case '&':
                        isSlar = true; // スラー
                        break;
                    case 'o':
                        ++index;
                        string oct = GetNumeric.Get(commandText, ref index);
                        NowStatus.Instance.NowOctave = int.Parse(oct);
                        break;
                    case 'r':
                        ++index;
                        string rest = GetNumeric.Get(commandText, ref index);
                        int restPoke = LengthConverter.ConvertToPokecom(rest);
                        if (!isSlar)
                        {
                            commands.Add(OperatorConstant.COMMAND_REST);
                            commands.Add((byte)restPoke);
                        }
                        else
                        {
                            commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] + restPoke);
                            isSlar = false;
                        }
                        break;
                    case 'b':
                        note += 11;
                        var noteInfob = GetNoteInfo(command, commandText, note, ref index);
                        if (!isSlar)
                        {
                            // 音終端カット
                            if (NowStatus.Instance.NowNote == prevNote && commands.Count >= 2)
                            {
                                commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] - 1);
                                commands.Add(OperatorConstant.COMMAND_REST);
                                commands.Add(1);
                            }

                            commands.AddRange(noteInfob);
                        }
                        else
                        {
                            commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] + noteInfob[noteInfob.Count - 1]);
                            isSlar = false;
                        }

                        break;
                    case 'a':
                        note += 9;
                        var noteInfoa = GetNoteInfo(command, commandText, note, ref index);
                        if (!isSlar)
                        {
                            // 音終端カット
                            if (NowStatus.Instance.NowNote == prevNote && commands.Count >= 2)
                            {
                                commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] - 1);
                                commands.Add(OperatorConstant.COMMAND_REST);
                                commands.Add(1);
                            }

                            commands.AddRange(noteInfoa);
                        }
                        else
                        {
                            commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] + noteInfoa[noteInfoa.Count - 1]);
                            isSlar = false;
                        }
                        break;
                    case 'g':
                        note += 7;
                        var noteInfog = GetNoteInfo(command, commandText, note, ref index);
                        if (!isSlar)
                        {
                            // 音終端カット
                            if (NowStatus.Instance.NowNote == prevNote && commands.Count >= 2)
                            {
                                commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] - 1);
                                commands.Add(OperatorConstant.COMMAND_REST);
                                commands.Add(1);
                            }

                            commands.AddRange(noteInfog);
                        }
                        else
                        {
                            commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] + noteInfog[noteInfog.Count - 1]);
                            isSlar = false;
                        }
                        break;
                    case 'f':
                        note += 5;
                        var noteInfof = GetNoteInfo(command, commandText, note, ref index);
                        if (!isSlar)
                        {
                            // 音終端カット
                            if (NowStatus.Instance.NowNote == prevNote && commands.Count >= 2)
                            {
                                commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] - 1);
                                commands.Add(OperatorConstant.COMMAND_REST);
                                commands.Add(1);
                            }

                            commands.AddRange(noteInfof);
                        }
                        else
                        {
                            commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] + noteInfof[noteInfof.Count - 1]);
                            isSlar = false;
                        }
                        break;
                    case 'e':
                        note += 4;
                        var noteInfoe = GetNoteInfo(command, commandText, note, ref index);
                        if (!isSlar)
                        {
                            // 音終端カット
                            if (NowStatus.Instance.NowNote == prevNote && commands.Count >= 2)
                            {
                                commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] - 1);
                                commands.Add(OperatorConstant.COMMAND_REST);
                                commands.Add(1);
                            }

                            commands.AddRange(noteInfoe);
                        }
                        else
                        {
                            commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] + noteInfoe[noteInfoe.Count - 1]);
                            isSlar = false;
                        }
                        break;
                    case 'd':
                        note += 2;
                        var noteInfod = GetNoteInfo(command, commandText, note, ref index);
                        if (!isSlar)
                        {
                            // 音終端カット
                            if (NowStatus.Instance.NowNote == prevNote && commands.Count >= 2)
                            {
                                commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] - 1);
                                commands.Add(OperatorConstant.COMMAND_REST);
                                commands.Add(1);
                            }

                            commands.AddRange(noteInfod);
                        }
                        else
                        {
                            commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] + noteInfod[noteInfod.Count - 1]);
                            isSlar = false;
                        }
                        break;
                    case 'c':
                        var noteInfoc = GetNoteInfo(command, commandText, note, ref index);
                        if (!isSlar)
                        {
                            // 音終端カット
                            if (NowStatus.Instance.NowNote == prevNote && commands.Count >= 2)
                            {
                                commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] - 1);
                                commands.Add(OperatorConstant.COMMAND_REST);
                                commands.Add(1);
                            }

                            commands.AddRange(noteInfoc);
                        }
                        else
                        {
                            commands[commands.Count - 1] = (byte)(commands[commands.Count - 1] + noteInfoc[noteInfoc.Count - 1]);
                            isSlar = false;
                        }
                        break;
                    case '<':
                        NowStatus.Instance.NowOctave--;
                        break;
                    case '>':
                        NowStatus.Instance.NowOctave++;
                        break;
                    default:
                        break;
                }
            }
            return commands;
        }

        private static List<byte> GetNoteInfo(char command, string commandText, int note, ref int index)
        {
            ++index;

            // 音高さ
            note += GetModulationNote.Get(command, commandText, ref index);

            // 音長さ
            string noteLengthStr = GetNumeric.Get(commandText, ref index);
            int noteLength = LengthConverter.ConvertToPokecom(noteLengthStr);

            List<byte> noteInfos = new List<byte>();
            if (!isOutputNoteIsRest)
            {
                while (note >= OperatorConstant.COMMAND_REST)
                {
                    note -= 12;
                }
                noteInfos.Add((byte)note);
            }
            else
            {
                noteInfos.Add(OperatorConstant.COMMAND_REST);
            }
            noteInfos.Add((byte)noteLength);

            // 現在の音階更新
            NowStatus.Instance.NowNote = note;

            return noteInfos;
        }
    }
}
