﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMLToPockecom
{
    static internal class GetNumeric
    {
        public static string Get(string text, ref int index)
        {
            string numText = string.Empty;
            for ( ; index < text.Length; index++)
            {
                char c = text[index];
                if (c >= '0' && c <= '9')
                {
                    numText += c;
                }
                else if (c == '.')
                {
                    numText += c;
                }
                else
                {
                    index--;
                    return numText;
                }
            }
            return numText;
        }
    }
}
