﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMLToPockecom
{
    internal class NowStatus
    {
        private static NowStatus instance;

        public static NowStatus Instance 
        {
            get 
            { 
                if (instance == null) 
                { 
                    instance = new NowStatus(); 
                }
                return instance;
            }
        }

        /// <summary>
        /// 現在のオクターブ
        /// </summary>
        public int NowOctave { get; set; } = 3;

        /// <summary>
        /// 現在の音階
        /// </summary>
        public int NowNote { get; set; } = 0x50;

        /// <summary>
        /// 現在の転調状態 -1フラット +1シャープ
        /// </summary>
        public int NowCNote { get; set; }

        /// <summary>
        /// 現在の転調状態 -1フラット +1シャープ
        /// </summary>
        public int NowDNote { get; set; }

        /// <summary>
        /// 現在の転調状態 -1フラット +1シャープ
        /// </summary>
        public int NowENote { get; set; }

        /// <summary>
        /// 現在の転調状態 -1フラット +1シャープ
        /// </summary>
        public int NowFNote { get; set; }

        /// <summary>
        /// 現在の転調状態 -1フラット +1シャープ
        /// </summary>
        public int NowGNote { get; set; }

        /// <summary>
        /// 現在の転調状態 -1フラット +1シャープ
        /// </summary>
        public int NowANote { get; set; }

        /// <summary>
        /// 現在の転調状態 -1フラット +1シャープ
        /// </summary>
        public int NowBNote { get; set; }

        public void ResetNote()
        {
            NowCNote = 0;
            NowDNote = 0;
            NowENote = 0;
            NowFNote = 0;
            NowGNote = 0;
            NowANote = 0;
            NowBNote = 0;
        }
    }
}
