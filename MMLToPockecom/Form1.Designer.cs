﻿namespace MMLToPockecom
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.mmlTextBox = new System.Windows.Forms.TextBox();
            this.transButton = new System.Windows.Forms.Button();
            this.resultTextbox = new System.Windows.Forms.TextBox();
            this.checkAllRest = new System.Windows.Forms.CheckBox();
            this.octaveTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mmlTextBox
            // 
            this.mmlTextBox.Location = new System.Drawing.Point(37, 53);
            this.mmlTextBox.Multiline = true;
            this.mmlTextBox.Name = "mmlTextBox";
            this.mmlTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mmlTextBox.Size = new System.Drawing.Size(375, 354);
            this.mmlTextBox.TabIndex = 0;
            // 
            // transButton
            // 
            this.transButton.Location = new System.Drawing.Point(162, 413);
            this.transButton.Name = "transButton";
            this.transButton.Size = new System.Drawing.Size(118, 24);
            this.transButton.TabIndex = 1;
            this.transButton.Text = "変換";
            this.transButton.UseVisualStyleBackColor = true;
            this.transButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // resultTextbox
            // 
            this.resultTextbox.Location = new System.Drawing.Point(418, 53);
            this.resultTextbox.Multiline = true;
            this.resultTextbox.Name = "resultTextbox";
            this.resultTextbox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.resultTextbox.Size = new System.Drawing.Size(375, 354);
            this.resultTextbox.TabIndex = 2;
            // 
            // checkAllRest
            // 
            this.checkAllRest.AutoSize = true;
            this.checkAllRest.Location = new System.Drawing.Point(377, 413);
            this.checkAllRest.Name = "checkAllRest";
            this.checkAllRest.Size = new System.Drawing.Size(125, 16);
            this.checkAllRest.TabIndex = 3;
            this.checkAllRest.Text = "全部休符で出力する";
            this.checkAllRest.UseVisualStyleBackColor = true;
            // 
            // octaveTextBox
            // 
            this.octaveTextBox.Location = new System.Drawing.Point(124, 10);
            this.octaveTextBox.Name = "octaveTextBox";
            this.octaveTextBox.Size = new System.Drawing.Size(100, 19);
            this.octaveTextBox.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "基準オクターブ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "MML";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(427, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "変換結果";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 462);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.octaveTextBox);
            this.Controls.Add(this.checkAllRest);
            this.Controls.Add(this.resultTextbox);
            this.Controls.Add(this.transButton);
            this.Controls.Add(this.mmlTextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox mmlTextBox;
        private System.Windows.Forms.Button transButton;
        private System.Windows.Forms.TextBox resultTextbox;
        private System.Windows.Forms.CheckBox checkAllRest;
        private System.Windows.Forms.TextBox octaveTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

