﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMLToPockecom
{
    internal static class LengthConstant
    {
        public static int NOTE_NUM = 12; // Noteの数

        public static int LENGTH_1 = 32; // 全音符の長さ
        public static int LENGTH_2 = 16; // 2分音符の長さ
        public static int LENGTH_4 = 8; // 4分音符の長さ
        public static int LENGTH_8 = 4; // 8分音符の長さ
        public static int LENGTH_16 = 2; // 16分音符の長さ
        public static int LENGTH_32 = 1; // 32分音符の長さ
    }
}
