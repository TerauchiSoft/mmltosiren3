﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMLToPockecom
{
    internal static class GetModulationNote
    {
        public static int Get(char note, string commandText, ref int index)
        {
            char value = commandText[index];

            if (value == '+')
            {
                switch (note)
                {
                    case 'c':
                        NowStatus.Instance.NowCNote = 1;
                        break;
                    case 'd':
                        NowStatus.Instance.NowDNote = 1;
                        break;
                    case 'e':
                        NowStatus.Instance.NowENote = 1;
                        break;
                    case 'f':
                        NowStatus.Instance.NowFNote = 1;
                        break;
                    case 'g':
                        NowStatus.Instance.NowGNote = 1;
                        break;
                    case 'a':
                        NowStatus.Instance.NowANote = 1;
                        break;
                    case 'b':
                        NowStatus.Instance.NowBNote = 1;
                        break;
                }
                index++;
            }
            else if (value == '-')
            {
                switch (note)
                {
                    case 'c':
                        NowStatus.Instance.NowCNote = 1;
                        break;
                    case 'd':
                        NowStatus.Instance.NowDNote = 1;
                        break;
                    case 'e':
                        NowStatus.Instance.NowENote = 1;
                        break;
                    case 'f':
                        NowStatus.Instance.NowFNote = 1;
                        break;
                    case 'g':
                        NowStatus.Instance.NowGNote = 1;
                        break;
                    case 'a':
                        NowStatus.Instance.NowANote = 1;
                        break;
                    case 'b':
                        NowStatus.Instance.NowBNote = 1;
                        break;
                }
                index++;
            }
            else if (value == '=')
            {
                switch (note)
                {
                    case 'c':
                        NowStatus.Instance.NowCNote = 0;
                        break;
                    case 'd':
                        NowStatus.Instance.NowDNote = 0;
                        break;
                    case 'e':
                        NowStatus.Instance.NowENote = 0;
                        break;
                    case 'f':
                        NowStatus.Instance.NowFNote = 0;
                        break;
                    case 'g':
                        NowStatus.Instance.NowGNote = 0;
                        break;
                    case 'a':
                        NowStatus.Instance.NowANote = 0;
                        break;
                    case 'b':
                        NowStatus.Instance.NowBNote = 0;
                        break;
                }
                index++;
            }

            switch (note)
            {
                case 'c':
                    return NowStatus.Instance.NowCNote;
                case 'd':
                    return NowStatus.Instance.NowDNote;
                case 'e':
                    return NowStatus.Instance.NowENote;
                case 'f':
                    return NowStatus.Instance.NowFNote;
                case 'g':
                    return NowStatus.Instance.NowGNote;
                case 'a':
                    return NowStatus.Instance.NowANote;
                case 'b':
                    return NowStatus.Instance.NowBNote;
                default:
                    return 0;
            }
        }
    }
}
